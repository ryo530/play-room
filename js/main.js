/* main.js */

// デバッグ用
function addLog(log) {
    let logs = $('#funcLog').html();
    logs = logs + '<br>' + log;
    $('#funcLog').html(logs);
}

function notify(message) {
    $('.notify').text(message);
    $('.notify').removeClass('notify-anim');
    $('.notify').addClass('notify-anim');
}

// ユーザー情報の描画
function setDetails(u) {
    alert(u);
    $('#gName').text(u.displayName);
    $('#gEmail').text(u.email);
    $('#gPhoto').css('src', u.photoURL);
    $('#gUid').text(u.uid);
}