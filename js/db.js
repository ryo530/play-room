
// cloud firestore controller
// return 0:success / 1:failed

var fsInsert = (collection, value) => {
    if (!collection) return 1;
    db.collection(collection).add(value);
}

var fsUserInsert = (doc, value) => {
    if (!doc) return 1;
    db.collection('users').doc(doc).set(value)
    .then(() => {
        alert('success');
    });
}