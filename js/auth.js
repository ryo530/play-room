var obj = null;
/** Google Authorization */

$(() => {

    // 認証初期処理・関数定義
    var AuthUI = {
      init: ()  =>{
        addLog('init');
        AuthUI.provider = new firebase.auth.GoogleAuthProvider();
        AuthUI.elAuthBtn = $('#authBtn');
        AuthUI.draw();
        // ログインボタン押下
        AuthUI.elAuthBtn.on('click', () => {
          addLog('elAuthBtn click');
          AuthUI.doAuth();
        });
        // firebase.auth().getRedirectResult().then( (result) => {
        //   addLog('getRedirectResult');
        //   if (result.user) {
        //     alert('logined')
        //     AuthUI.data.authed = true;
        //     AuthUI.draw();
        //   } else {
        //     alert('not logined');
        //   }
        // }).catch((error) => {
        //   // Handle Errors here.
        //   var errorCode = error.code;
        //   var errorMessage = error.message;
        //   // The email of the user's account used.
        //   var email = error.email;
        //   // The firebase.auth.AuthCredential type that was used.
        //   var credential = error.credential;
        //   // ...
        //   alert('error01');
        // });
        firebase.auth().onAuthStateChanged( (user) => {
          addLog('onAuthStateChanged');
          if (user) {
            alert(user);
            obj = user;
            AuthUI.data.authed = true;
            AuthUI.data.user = user;
            AuthUI.draw();
          } else {
            alert('未ログイン');
          }
        });
      },
      data: {
        authed: false,
        user: ''
      },
      // 画面へ描画
      draw: () => {
        addLog('draw');
        // ログイン済みの場合
        if (AuthUI.data.authed) {
          // main.js ユーザ情報の描画
          setDetails(AuthUI.data.user);
          addLog('setDetails');
        }
        // ログインボタンの制御
        AuthUI.elAuthBtn.text(AuthUI.data.authed ? 'LOGOUT' : 'LOGIN');
      },
      // 認証
      doAuth: () => {
        addLog('do auth');
        if (AuthUI.data.authed) {
          // ログイン済みの場合
          firebase.auth().signOut().then(() => {
            addLog('sign out');
            AuthUI.data.authed = false;
            AuthUI.data.userId = '';
            AuthUI.draw();
          }, (error) => {
            // An error happened.
            alert('error02');
          });
        } else {
          // 未ログインの場合
          firebase.auth().signInWithRedirect(AuthUI.provider);
          addLog('signInWithRedirect');
        }
      }
    };
    AuthUI.init();
    addLog('AuthUI.init');
  
  });